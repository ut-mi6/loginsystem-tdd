﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
namespace loginSystem
{
    public partial class register : System.Web.UI.Page
    {
        dbConnect db = new dbConnect();

        protected void registerBtn_Click(object sender, EventArgs e)
        {
            if (!(notBlank(firstName.Text) && notBlank(firstName.Text) && notBlank(email.Text) && notBlank(password.Text)))
            {
                message.Text = "Some of fields are required!";
                mes.Visible = true;
            }
            else if (! passworDigits(password.Text))
            {
                message.Text = "Password must be at least 7 digits!";
                mes.Visible = true;
            }
            else if (! emailIsValid(email.Text))
            {
                message.Text = "Email is not valid!";
                mes.Visible = true;
            }
            else
            {
                string f = db.postData(string.Format("insert into [logins] (name,lastName, email, password) values ('{0}','{1}','{2}','{3}')", firstName.Text, lastName.Text, email.Text, password.Text), dbname: "TDD");
                if(f == "1")
                {
                    DataView dv = db.Getdata("select top 1 id from logins order by id desc", dbname: "TDD");
                    Response.Redirect("index.aspx?id=" + dv[0]["id"]);
                }
                else
                {
                    message.Text = "Error in the database!";
                    mes.Visible = true;
                }

             
            }

        }

        public bool notBlank(string val)
        {
            if (val == null || val == "") return false;
            return true;
        }

        public bool passworDigits(string val)
        {
            if (val.Length < 8) return false;
            return true;          
        }

        public bool emailIsValid(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

    }
}