﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="loginSystem.register" %>

<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from brandio.io/envato/iofrm/html/register1.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 03 Nov 2018 13:15:23 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iofrm</title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme1.css">
</head>
<body>
    <div class="form-body">
        <div class="website-logo">
            <a href="index.html">
                <div class="logo">
                    <%-- <img class="logo-size" src="images/logo-light.svg" alt="">--%>
                </div>
            </a>
        </div>
        <div class="row">
            <div class="img-holder">
                <div class="bg"></div>
                <div class="info-holder">
                </div>
            </div>
            <div class="form-holder">
                <div class="form-content">
                    <div class="form-items">
                        <h3>Welcome to IM6</h3>
                        <p>Register for new account</p>
                        <div class="alert alert-danger" id="mes" runat="server" visible="false">
                            <strong>
                                <asp:Label ID="message" runat="server"></asp:Label> <br />
                            </strong>
                        </div>
                        <form id="form1" runat="server">
                            <asp:TextBox ID="firstName" runat="server" placeholder="First Name"></asp:TextBox>
                            <asp:TextBox ID="lastName" runat="server" placeholder="Last Name"></asp:TextBox>
                            <asp:TextBox ID="email" runat="server" placeholder="Email"></asp:TextBox>
                            <asp:TextBox ID="password" runat="server" TextMode="Password" placeholder="Password"></asp:TextBox>
                            <asp:Button ID="registerBtn" runat="server" CssClass="ibtn bg-secondary text-white" Text="Register" OnClick="registerBtn_Click" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
</body>
</html>
