﻿using System;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace loginSystem
{
    public class dbConnect
    {
        private string constr;
        public string getConstr {
            get
            {
                return constr;
            }
        }

        SqlConnection cn = new SqlConnection();
        SqlCommand cm = new SqlCommand();
        SqlDataSource ad = new SqlDataSource();

        public void prepad(string dbname)
        {
            cm.Connection = cn;
            try
            {
                string con = ConfigurationManager.ConnectionStrings[dbname].ConnectionString;

                if (cn.State == ConnectionState.Closed)
                {
                    cn.ConnectionString = con;
                }
                else if (cn.ConnectionString != con)
                {
                    cn.Close();
                    cn.ConnectionString = con;
                }

                ad.ConnectionString = con;
                constr = con;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write("dbname is " + dbname);
            }
        }

        public DataView Getdata(string str, string dbname)
        {
            DataView tbl = null;
            prepad(dbname);
            ad.SelectCommand = str;
            try
            {
                tbl = (DataView)ad.Select(DataSourceSelectArguments.Empty);
            }
            catch (Exception ex)
            {
            }
            return tbl;
        }

        public string postData(string str, string dbname)
        {
            prepad(dbname);
            cm.CommandText = str;
            if (cm.Connection.State == ConnectionState.Closed) cm.Connection.Open();
            try
            {
                return cm.ExecuteNonQuery().ToString();
            }
            catch (Exception ex)
            {
                return "Command:" + str + "<br>Error:" + ex.Message;
            }
            cn.Close();
        }
    }

}