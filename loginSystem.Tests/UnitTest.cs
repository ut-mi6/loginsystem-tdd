﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using loginSystem;
using System.Configuration;

namespace Tests
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void prepad()
        {
            //1 Arrange
            dbConnect db = new dbConnect();
            //2 Act
            db.prepad("TDD");
            // 3 Assert
            Assert.AreEqual("Data Source=DESKTOP-UOBEK67\\SQLEXPRESS;Initial Catalog=TDD;Integrated Security=True", db.getConstr);
        }



        [TestMethod]
        public void notBlank()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.notBlank("");
            // 3 Assert
            Assert.AreEqual(false, ret);
        }

        [TestMethod]
        public void blank()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.notBlank("notBlankValue");
            // 3 Assert
            Assert.AreEqual(true, ret);
        }

        [TestMethod]
        public void passworDigits()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.passworDigits("");
            // 3 Assert
            Assert.AreEqual(false, ret);
        }

        [TestMethod]
        public void passworCount()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.passworDigits("123456789");
            // 3 Assert
            Assert.AreEqual(true, ret);
        }


        [TestMethod]
        public void emailNOTValid()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.emailIsValid("blaaa");
            Assert.AreEqual(false, ret);
        }

        [TestMethod]
        public void emailIsValid()
        {
            //1 Arrange
            register register = new register();
            //2 Act
            bool ret = register.emailIsValid("ibrahim@gmail.com");
            Assert.AreEqual(true, ret);
        }
    }
}
